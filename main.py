from flask import Flask, request, redirect, url_for, flash, send_file
import os
from werkzeug.utils import secure_filename
import pandas as pd
import us


UPLOAD_FOLDER = '/tmp'
ALLOWED_EXTENSIONS = {'csv', 'xlsx'}

from fuzzywuzzy import fuzz
import us

def process_data(file_name):
    df = pd.read_excel(file_name)
    # Remove Duplicates Rows
    df.drop_duplicates(inplace=True)
    # Add Address 1, Address 2, city, state, zip, country columns
    df['Address 1'] = df['Address 1'] + ', ' + df['Address 2'] 
    # Remove Address 2 column
    df.drop('Address 2', axis=1, inplace=True)
    # Rename Address 1 column to Address
    df.rename(columns={'Address 1': 'Address'}, inplace=True)
    # Remove extra spaces from state column
    df['State'] = df['State'].str.strip()
    # Remove . from state column
    df['State'] = df['State'].str.replace('.', '')
    # Apply state_abbr function to state column
    df['State'] = df['State'].apply(state_abbr)

    return df


# US State Abbreviations
state_ab = ['MA', 'NY', 'CA', 'TX', 'FL', 'WA', 'IL', 'OH', 'GA', 'NC', 'MI', 'NJ', 'VA', 'TN', 'AZ', 'IN', 'MO', 'MD', 'WI', 'MN', 'CO', 'AL', 'SC', 'LA', 'KY', 'OR', 'OK', 'CT', 'PR', 'IA', 'MS', 'AR', 'UT', 'KS', 'NV', 'NM', 'NE', 'WV', 'ID', 'HI', 'NH', 'ME', 'MT', 'RI', 'DE', 'SD', 'ND', 'AK', 'VT', 'WY', 'DC']

# US State Names
state_names = ['Massachusetts', 'New York', 'California', 'Texas', 'Florida', 'Washington', 'Illinois', 'Ohio', 'Georgia', 'North Carolina', 'Michigan', 'New Jersey', 'Virginia', 'Tennessee', 'Arizona', 'Indiana', 'Missouri', 'Maryland', 'Wisconsin', 'Minnesota', 'Colorado', 'Alabama', 'South Carolina', 'Louisiana', 'Kentucky', 'Oregon', 'Oklahoma', 'Connecticut', 'Puerto Rico', 'Iowa', 'Mississippi', 'Arkansas', 'Utah', 'Kansas', 'Nevada', 'New Mexico', 'Nebraska', 'West Virginia', 'Idaho', 'Hawaii', 'New Hampshire', 'Maine', 'Montana', 'Rhode Island', 'Delaware', 'South Dakota', 'North Dakota', 'Alaska', 'Vermont', 'Wyoming', 'District of Columbia']
state_full_list = state_ab + state_names


from fuzzywuzzy import fuzz

def phonetic_match(name, names_list, threshold=40):
  # Iterate through the names in the list and compute the fuzzy match score
  for n in names_list:
    score = fuzz.token_sort_ratio(name, n)
    # If the score is above the threshold, return the match
    if score >= threshold:
      return n
  # If no match is found, return None
  return None

# Get State Abbreviation
def state_abbr(state_value):
    state= us.states.lookup(state_value)
    if state is None:
        state = phonetic_match(state_value, state_full_list)
        if state is None:
            state = 'Not Found'
            output= state
        else:
            state = us.states.lookup(state)
            output= state.abbr
    else: 
        output= state.abbr
    return output


app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
# create a flask app
# @app.route('/')
# def home():
#     return 'Redirect to /upload'

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            # process the file and save it to a temporary location
            filename = secure_filename(file.filename)
            file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(file_path)
            # Apply process_data function to file
            df = process_data(file_path)
            # remove the file from the temporary location
            os.remove(file_path)
            # Save file to a temporary location
            # Save output file to a temporary location
            out_file_path = os.path.join(app.config['UPLOAD_FOLDER'], 'output_' + filename)

            df.to_excel(out_file_path, index=False)
            
            # return redirect(url_for('upload_file',
            #                         filename=filename))

            return send_file(out_file_path, as_attachment=True)
        # Remove output file from temporary location
        os.remove(out_file_path)
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''


# @app.route('/download/<filename>')
# def download_file(filename):
#     return send_file(os.path.join(app.config['UPLOAD_FOLDER'], filename), as_attachment=True)

if (__name__ == '__main__'):
    app.run(debug=True)

